﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kaspi.Business.WebOperator.Code
{
    public class OperationJsonResult : JsonResult
    {
        public bool Success { get; }

        public string Message { get; }

        public object Content { get; }

        public object Action { get; }

        public OperationJsonResult(bool success, string message, object action, object content)
        {
            Action = action;
            Content = content;
            Message = message;
            Success = success;

            Data = new
            {
                success, message, action, content
            };
        }
    }
}