﻿using System.Web.Mvc;

namespace Kaspi.Business.WebOperator.Code
{
    public class BaseController : Controller
    {
        public OperationJsonResult Operation(bool success = true, string message = "", object data = null, object action = null)
        {
            return new OperationJsonResult(success, message, data, action);
        }
    }
}