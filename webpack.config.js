/// <binding ProjectOpened='Watch - Development' />
"use strict";

// Плагин для очистки выходной папки (bundle) перед созданием новой
const NotifierPlugin = require('webpack-notifier');

module.exports = {
    entry: "./assets/core/startup.js",
    output: {
        filename: "./dist/script.js"
    }, 
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: /node_modules/, 
                options: {
                    presets: ["es2015"]
                }
            },
            {
                test: /\.vue$/,
                loader: "vue-loader"
            }
        ]
    },
    resolve: {
      extensions: ["*", ".js", ".vue"]  
    },
    plugins: [
        new NotifierPlugin()
    ]
};