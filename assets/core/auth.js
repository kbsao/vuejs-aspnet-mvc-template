﻿const localStorageKey = "currentuser";

export default {
    currentUser() {
        return localStorage.getItem(localStorageKey);
    },

    isAuthenticated() {
        return localStorageKey in localStorage;
    },

    authenticateWith(user) {
        localStorage.setItem(localStorageKey, user);
    }
}