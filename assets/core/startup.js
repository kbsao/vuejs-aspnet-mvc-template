﻿/* Imports */
import Vue from "vue";
import VueRouter from "vue-router"

import App from "./app.vue";

import componentNavigation from "../component/navigation.vue";

import pageHome from "../page/home.vue";
import pageError from "../page/error.vue";

/* Configurations */
Vue.use(VueRouter);

Vue.config.debug = true;
Vue.config.devtools = true;

/* Router */
const router = new VueRouter({
    routes: [ 
        {
            path: "/",
            component: pageHome
        },
        {
            path: "/organizations",
            component: pageHome
        },
        {
            path: "/error/:code",
            component: pageError
        },
        { 
            path: "*",
            redirect: "/"
        }
    ]
});

/* Instance */
new Vue({ 
    router,
    render: h => h(App),
    created() {
        console.log("created");
    }, 
    mounted() {
        console.log("mounted");
    }
}).$mount("#app");  