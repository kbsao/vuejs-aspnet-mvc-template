﻿using System.Web.Mvc;
using Kaspi.Business.WebOperator.Code;

namespace Kaspi.Business.WebOperator.Controllers
{
    public class AuthController : BaseController
    {
        public OperationJsonResult Authenticate()
        {
            return Operation(data: User.Identity.Name);
        }
    }
}