﻿using System.Web;
using System.Web.Optimization;

namespace Kaspi.Business.WebOperator
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            var scriptBundle = new ScriptBundle("~/bundle/script")
                .Include("~/dist/script.js");
            bundles.Add(scriptBundle);

            var styleBundle = new StyleBundle("~/bundle/style") // TODO: Consider using Webpack
                .IncludeDirectory("~/Assets/vendor/", "*.css");
            bundles.Add(styleBundle);
        }
    }
}
